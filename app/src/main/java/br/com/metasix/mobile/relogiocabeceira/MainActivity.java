package br.com.metasix.mobile.relogiocabeceira;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ViewHolder mViewHolder = new ViewHolder();
    private Handler mHandler = new Handler();
    private Runnable mRunable;
    private boolean mRunnableStoped = false;
    private boolean mIsBatteryOn = true;

    private BroadcastReceiver mBatteryReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int level  = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            mViewHolder.mTextLevel.setText(String.valueOf(level) + "%");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        this.mViewHolder.mTextHourMinute = (TextView) this.findViewById(R.id.text_hour_minute);
        this.mViewHolder.mTextSegunds = (TextView) this.findViewById(R.id.text_seg);
        this.mViewHolder.mTextLevel = (TextView) this.findViewById(R.id.text_battery_Level);
        this.mViewHolder.mCheckBattery = (CheckBox) this.findViewById(R.id.check_battery);
        this.mViewHolder.mImageOption = (ImageView) this.findViewById(R.id.imgSettings);
        this.mViewHolder.mImageClose = (ImageView) this.findViewById(R.id.img_clear);
        this.mViewHolder.mLinearOptions = (LinearLayout) this.findViewById(R.id.linearOptions);


        // não bloqueia a tela enquanto esta na aplicação
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // não aparece a barra de status
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        this.registerReceiver(mBatteryReciver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        this.mViewHolder.mCheckBattery.setChecked(true);
        this.setListener();

        this.mViewHolder.mLinearOptions.animate().translationY(500);


    }

    private void setListener() {

    this.mViewHolder.mCheckBattery.setOnClickListener(this);
    this.mViewHolder.mImageOption.setOnClickListener(this);
    this.mViewHolder.mImageClose.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        if(id == R.id.check_battery){
            this.toggleCheckBattery();

        }else if(id ==R.id.imgSettings){

            // Abre menu
            this.mViewHolder.mLinearOptions.setVisibility(View.VISIBLE);
            this.mViewHolder.mLinearOptions.animate()
                    .translationY(0)
                    .setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));

        }else if(id == R.id.img_clear){
            // Fecha menu

            this.mViewHolder.mLinearOptions.animate()
                    .translationY(this.mViewHolder.mLinearOptions.getMeasuredHeight())
                    .setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));

        }

    }

    private void toggleCheckBattery() {

        if (this.mIsBatteryOn){

            this.mIsBatteryOn = false;
            //oculta o componente
            this.mViewHolder.mTextLevel.setVisibility(View.GONE);

        }else{

            this.mIsBatteryOn = true;
            //deixa visivel o componente
            this.mViewHolder.mTextLevel.setVisibility(View.VISIBLE);

        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        this.mRunnableStoped = false;
        this.startBedside();

    }

    @Override
    protected void onStop() {
        super.onStop();
        this.mRunnableStoped = true;
    }

    private void startBedside() {

        final Calendar calendar = Calendar.getInstance();

        this.mRunable = new Runnable() {
            @Override
            public void run() {


                if (mRunnableStoped)
                    return;

                calendar.setTimeInMillis(System.currentTimeMillis());

                String hourMinutesFormat = String.format("%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
                String hourSecund= String.format("%02d", calendar.get(Calendar.SECOND));

                mViewHolder.mTextHourMinute.setText(hourMinutesFormat);
                mViewHolder.mTextSegunds.setText(hourSecund);


                long now = SystemClock.uptimeMillis();
                long next = now + (1000 - (now % 1000));

                mHandler.postAtTime(mRunable, next);

            }
        };
        this.mRunable.run();



        //



    }



    private static class ViewHolder{

        TextView mTextHourMinute;
        TextView mTextSegunds;
        TextView mTextLevel;
        CheckBox mCheckBattery;
        ImageView mImageOption;
        ImageView mImageClose;
        LinearLayout mLinearOptions;


    }
}
